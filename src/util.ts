/**
 * File: /src/util.ts
 * Project: @risserlabs/auto-contrast
 * File Created: 20-08-2022 11:42:08
 * Author: Clay Risser
 * -----
 * Last Modified: 05-09-2022 23:22:09
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { StyleProp, StyleSheet } from "react-native";

export function lookupStyle<T>(
  style: StyleProp<unknown> | undefined,
  key: string
): T | undefined {
  if (typeof style === "undefined") return;
  const flattenedStyle = StyleSheet.flatten(style);
  if (typeof flattenedStyle !== "object") return;
  return (flattenedStyle as Record<string, unknown>)[key] as T;
}
