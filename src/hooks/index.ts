/**
 * File: /src/hooks/index.ts
 * Project: auto-contrast
 * File Created: 02-03-2022 03:00:24
 * Author: Clay Risser
 * -----
 * Last Modified: 13-06-2022 01:19:01
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import useAutoContrast from "./useAutoContrast";
import useBackgroundColor from "./useBackgroundColor";
import useCalculatedSx from "./useCalculatedSx";
import useColor from "./useColor";
import useThemeLookup from "./useThemeLookup";

export {
  useAutoContrast,
  useBackgroundColor,
  useCalculatedSx,
  useColor,
  useThemeLookup,
};
