/**
 * File: /src/components/BackgroundColorProvider.ts
 * Project: auto-contrast
 * File Created: 02-03-2022 03:02:05
 * Author: Clay Risser
 * -----
 * Last Modified: 02-03-2022 03:02:14
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import type { StyleProp } from "react-native";
import React, { FC, ReactNode } from "react";
import { Sx, SxProp } from "dripsy";
import BackgroundColorContext from "../contexts/backgroundColor";
import useCalculatedSx from "../hooks/useCalculatedSx";
import { lookupStyle } from "../util";

export interface BackgroundColorProviderProps {
  backgroundColor?: string;
  children: ReactNode;
  defaultStyle?: Sx;
  style?: StyleProp<unknown>;
  sx?: SxProp;
  themeKey?: string;
  [key: string]: unknown;
}

const BackgroundColorProvider: FC<BackgroundColorProviderProps> = (
  props: BackgroundColorProviderProps
) => {
  const sx = useCalculatedSx(props.sx || {});
  const backgroundColor =
    props.backgroundColor ||
    lookupStyle(props.style, "backgroundColor") ||
    sx?.backgroundColor ||
    sx?.bg ||
    props.defaultStyle?.backgroundColor ||
    props.defaultStyle?.bg;
  if (backgroundColor && backgroundColor !== "transparent") {
    return (
      <BackgroundColorContext.Provider
        value={{ color: backgroundColor, themeKey: props.themeKey }}
      >
        {props.children}
      </BackgroundColorContext.Provider>
    );
  }
  return <>{props.children}</>;
};

BackgroundColorProvider.defaultProps = {};

export default BackgroundColorProvider;
